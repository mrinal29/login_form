import React, { useState } from 'react'
import './LoginForm.css'

const LoginForm = () => {
    const [email , setEmail] = useState("");
    const [password , setPassword] = useState("")

    const emailChangeHandler = (e) => {
        setEmail(e.target.value)
    }
    const passwordChangeHandler = (e) => {
        setPassword(e.target.value)
    }
    const checkLoginCredential = () => {
        if (email == "mrinal@gmail.com" && password == "mrinal") {
            alert("congrats! you have logged in ")
            return false
            
        }
        else{
            alert("Details provided is not matching , try it again")
        }
    }
    return (
        <div className='container'>
            <h1 className='header'>Login Form</h1>
            <form action="">
                <div>
                    <label htmlFor="email">Email</label>
                    <input type="email" name='email' id='email'placeholder='email..' autoComplete='off' value={email} onChange = {emailChangeHandler} />
                </div>
                <div>
                    <label htmlFor="password">Password</label>
                    <input type="password" name='password' id='password'placeholder='password..' autoComplete='off' value={password} onChange = {passwordChangeHandler}/>
                </div>
                <div>
                    <button type='submit' onClick={checkLoginCredential}>Login</button>
                </div>
            </form>
        </div>
    )
}

export default LoginForm
